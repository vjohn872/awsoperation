from fastapi import FastAPI
from pydantic import BaseModel, Field
from typing import List, Set

app = FastAPI()

########Section 1 and 2 : Basic Setup and Routing and Parameters Pass Send via Request############

@app.get('/')
def index():
    return 'Hello there'

@app.get('/movies/{id}')
def movie(id):
    return {f'Property no : {id}'}


@app.get('/movies')
def movies():
    return {'movielist': {'movi1':'ddlj', 'movi2' : 'hssh'}}

@app.get('/products')
def products(id,price=None):
    return {f"Product id : {id}, Price : {price}"}

@app.get('/profile/{userid}/comments')
def profile(userid:int ,comment:str):
    return {f"User id : {userid}, Commment : {comment}"}


#######Section 3 : Request Body and Pydantic Models#############

class Product(BaseModel):
    name    : str
    price   : int = Field(title = "Price of Product", description = "Enter valid price", gt = 100)
    discount: int
    discounted_price : float

class User(BaseModel):
    name : str
    email : str

@app.post('/addproduct/{pid}')
def addproduct(product : Product, pid : int, category = "mobile"):
    product.discounted_price = product.price - (product.price * product.discount)/100
    return {
        "product" : product, 
        "product_id" : pid,
        "category" : category
    }

@app.post('/purchase')
def purchase(user : User, product : Product):
    product.discounted_price = product.price - (product.price * product.discount)/100

    return {"User is : " : user,
            "product : " : product
    }

#######Section 4 : Database Connectivity#############

