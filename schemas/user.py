def serializeDict(item) -> dict:
    return {
        'id' : str(item['_id']),
        'name' : item['name'],
        'email' : item['email'],
        'password--' : item["password"]
    }

def serializeList(entity) -> list:
    return [serializeDict(item) for item in entity]

def serializeDict(a) -> dict:
    return {**{i:str(a[i]) for i in a if i=='_id'}, **{i:a[i] for i in a if i!='_id'}}

def serializeList(entity:dict) -> list:
    return [serializeDict(a) for a in entity]

def user_helper(user) -> dict :
    return{
        'id' : str(user['_id']),
        'user_name' : user['name'],
        'email' : user['email'],
        'password' : user['password']
        }

def document_helper(document) -> dict :
    return{
        'id' : str(document['_id']),
        'site_id':document['siteid'],
        'company_id':document['companyid'],
        'document_title':document['document_title'],
        'documents':document['documents'],
        'message':document['message'],
        'user_group':document['user_group'],
        'files':document['files']
        }
    
def show_document(document) -> dict:
    return{
        'id' : str(document['_id']),
        'title' : document['document_title'],
        'files' : document['files']
    }