from fastapi import UploadFile
from pydantic import BaseModel, EmailStr, Field
from typing import List, Optional

class User(BaseModel):
    name : str = Field(title='Enter name')
    email : EmailStr = Field(title='Enter email')
    password : str = Field(title='Enter password')

class ShowUser(User):
    class Config():
        orm_mode = True
    
class UpdateUser(BaseModel):
    name : Optional[str]
    email : Optional[EmailStr]
    password : Optional[str] 

class Token(BaseModel):
    access_token : str
    token_type   : str

class TokenData(BaseModel):
    username : Optional[str] = None

class Document_Details(BaseModel):
    siteid : str = Field(title = 'Enter site id')
    companyid : str = Field(title = 'Enter company id')
    document_title : str = Field(title = 'Enter Title of the document')
    documents : str = Field(title = 'Enter which documents are going to upload')
    message  : str = Field(title = 'Enter message related to documents')
    user_group : str = Field(title = 'Enter particular group id')
    files : Optional[List[str]]


class ShowDocument_Details(BaseModel):

    orm_mode = True

class Document(BaseModel):
    username : str = Field(title = 'Enter username')
    url : List[str] = Field(title = 'Enter url of the document')