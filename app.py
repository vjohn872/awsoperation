import uvicorn
from fastapi import FastAPI
from routes.user import user
from routes.login import router
from routes.aws import aws
from routes.documents import documents
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI(docs_url = "/")

'''
origins = ['https://localhost:3000']
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
{} {}
'''
app.include_router(user)
app.include_router(router)
app.include_router(aws)
app.include_router(documents)



if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8002)
