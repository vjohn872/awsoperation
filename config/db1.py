from .db import document_detail_collection
from schemas.user import serializeList, serializeDict, user_helper, document_helper, show_document
from bson import ObjectId



async def create_document_detail(user_data : dict) -> dict:
    document_detail = await document_detail_collection.insert_one(user_data)
    new_document_detail = await document_detail_collection.find_one({'_id' : document_detail.inserted_id})
    return document_helper(new_document_detail)

async def fetch_all_document_detail():  
    try:
        print('---')
        documents = []
        async for document in document_detail_collection.find():
            print('----',document)
            documents.append(document_helper(document))
        print(documents)
    except Exception as e:
        print(str(e))
    return documents

async def fetch_document_byid(id : str):
    document = await document_detail_collection.find_one({'_id' : ObjectId(id)})
    if document:
        return show_document(document)

async def fetch_document_bycompanyid(companyid : str):
    print('----')
    document = await document_detail_collection.find_one({'companyid' : companyid})
    print(document)
    if document:
        return show_document(document)

async def fetch_document_bysiteid(siteid : str):
    print('----')
    document = await document_detail_collection.find_one({'siteid' : siteid})
    print(document)
    if document:
        return show_document(document)



async def fetch_document_byparamsid(siteid,title,company_id):
    print('----')
    document = await document_detail_collection.find_one({'siteid' : siteid, 'companyid' : company_id, 'title' : title})
    print(document)
    if document:
        return show_document(document)

#########################################################
