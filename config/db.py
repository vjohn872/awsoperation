from bson import ObjectId
import motor.motor_asyncio
from models.user import Document
from schemas.user import serializeList, serializeDict, user_helper, document_helper, show_document

MONGO_DETAILS = 'mongodb://localhost:27017'
client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.user
user_collection = database.User
document_collection = database.Document
document_detail_collection = database.Documents



async def fetch_all_users():  
    '''
    users = await user_collection.find().to_list(1000)
    return users
    '''
    try:
        users = []
        async for user in user_collection.find():
            users.append(user_helper(user))
    except Exception as e:
        print(str(e))
    return users
        
async def create_user(user_data : dict) -> dict:
    user = await user_collection.insert_one(user_data)
    new_user = await user_collection.find_one({'_id' : user.inserted_id})
    return serializeDict(new_user)

async def fetch_user(id : str):
    user = await user_collection.find_one({'_id' : ObjectId(id)})
    if user:
        return user

async def updated_user(id : str, data : dict):
    # Return false if an empty request body is sent.
    if len(data) < 1:
        return False
    user = await user_collection.find_one({"_id": ObjectId(id)})
    if user:
        updated_user = await user_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_user:
            return True
        return False
    
async def delete_user(id :str):
    user = await user_collection.find_one({'_id' : ObjectId(id)})
    if user:
        await user_collection.delete_one({'_id' : ObjectId(id)})
        return True

##############Document Details#############
'''
async def create_document_detail(user_data : dict) -> dict:
    document_detail = await document_detail_collection.insert_one(user_data)
    new_document_detail = await document_detail_collection.find_one({'_id' : document_detail.inserted_id})
    return serializeDict(new_document_detail)

async def fetch_all_document_detail():  
    try:
        print('---')
        documents = []
        async for document in document_detail_collection.find():
            print('----',document)
            documents.append(document_helper(document))
        print(documents)
    except Exception as e:
        print(str(e))
    return documents

async def fetch_document_byid(id : str):
    print('----')
    document = await document_detail_collection.find_one({'_id' : ObjectId(id)})
    print(document)
    if document:
        return show_document(document)
'''
#########################################################


############Create and Upload Documents to AWS############

async def create_document(user_data : dict) -> dict:
    document= await document_collection.insert_one(user_data)
    new_document= await document_collection.find_one({'_id' : document.inserted_id})
    return serializeDict(new_document)