from fastapi import APIRouter,  HTTPException, status, UploadFile, File, Depends
from typing import List
import shutil
import boto3
import os
from pprint import pprint
from dotenv import load_dotenv, find_dotenv
from .login import get_current_user
from models.user import Document, User
from fastapi.encoders import jsonable_encoder
from config.db import create_document
from models import user
import requests


aws = APIRouter(
    prefix = '/aws',
    tags = ["AWS Operation"]
)

load_dotenv(find_dotenv())

#######UPLOAD FILE TO AWS S3###########

access_key = os.getenv('access_key')
secret_access_key = os.getenv('secret_access_key')
bucket_name = os.getenv('bucket_name')
aws_region = os.getenv('region')

client = boto3.client(
        's3',
        aws_access_key_id = access_key,
        aws_secret_access_key = secret_access_key,
        region_name = aws_region      
    )

'''
 #fil_loc = os.path.join(os.getcwd(),'Upload/') + file.filename
    
    with open(fil_loc, 'wb') as file1:
        shutil.copyfileobj(file.file, file1)
    '''


####################################################################################

########################Creating Presigned Url######################################


@aws.post('/presigned-url')
async def presigned_urls(file : UploadFile = File(...) ,current_user : User = Depends(get_current_user)):

    fil_loc = os.path.join(os.getcwd(),'routes/') + file.filename
    
    with open(fil_loc, 'wb') as file1:
        shutil.copyfileobj(file.file, file1)
   
    object_name = file.filename

    response = client.generate_presigned_post(
        Bucket = bucket_name,
        Key = object_name,
        ExpiresIn = 3600
    )
    print(response)
    
    fil_loc = os.path.join(os.getcwd(),'routes/') + object_name
    files = {'file' : open(fil_loc,'rb')}
    r = requests.post(response['url'], data = response['fields'], files = files)
    print(r.status_code)
    
    response = client.generate_presigned_url(
        'get_object',
        Params = {
            'Bucket':'verma.s',
            'Key' : object_name
            },
        ExpiresIn = 36000
    )
    pprint(response)
    return {'Presigned-url' : response}


####################################################################################


@aws.post('/presigned-urls')
async def presigned_urlss(filename : str ,current_user : User = Depends(get_current_user)):
    try: 
        response = client.generate_presigned_url(
            'get_object',
            Params = {
                'Bucket':'verma.s',
                'Key' : filename
                },
            ExpiresIn = 36000
        )
    except Exception as e:
        print(str(e))
    pprint(response)
    return {'Presigned-url' : response}




####################UPLOADING FILES TO AWS S3 BUCKET################################

@aws.post('/upload-files', status_code=201)
async def create_documents(files : List[UploadFile] = File(...), current_user : User = Depends(get_current_user)):
    
    #files = os.path.join(os.getcwd(),'Media')
    paths = []
    print(files)
    for file in files:

        if not file:
            raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = "File not found")
        '''
        fil_loc = os.path.join(os.getcwd(),'Media/') + file.filename
        with open(fil_loc, 'wb') as file1:
            shutil.copyfileobj(file.file, file1)
        '''
        try:
            
            print('before uploading')
            print(file.file)
            client.upload_fileobj(
                   file.file,
                   bucket_name,
                   file.filename
                   )
            
            print('After uploading')
            print(file.filename)
            path = "s3://verma.s/" + file.filename
            paths.append(str(path))
            print(path)
        except Exception as e:
            print(str(e))
    print(current_user.get('name'))
    data = {'name' : current_user.get('name'), 'url' : paths}
    print(data)
    data = jsonable_encoder(data)
    document = await create_document(data)
    
    return {'detail' : document}
        
            
 ##############download files##############
    



@aws.post('/download-file/{filename}',status_code = status.HTTP_302_FOUND)
async def file_download(filename, current_user : User = Depends(get_current_user)): 

        try:
            client.download_file(bucket_name,filename,os.path.join('./FilesDownload/Files', filename))        
            return {'detail' : f'{filename} downloaded successfully'}
        except Exception as e:
            print(str(e))



@aws.post('/download-all-files',status_code = status.HTTP_302_FOUND)
async def file_download(current_user : User = Depends(get_current_user)): 
    list = client.list_objects(Bucket = bucket_name )['Contents']
    if not list:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = "Empty List")
    for key in list:
        client.download_file(bucket_name,key['Key'], os.path.join('./FilesDownload/AllFiles', key['Key']))
    return {'detail' : 'All files downloaded successfully'}

#############################################


'''
@aws.post('/upload-files')
async def file_upload():
    client = boto3.client(
        's3',
        aws_access_key_id = access_key,
        aws_secret_access_key = secret_access_key      
    )
    #######upload files#######

    files = os.path.join(os.getcwd(),'Media')
    print(files)
    for file in os.listdir(files):
        print(type(file))
        try:
            print('before uploading')
            client.upload_file(
                os.path.join(files,file),
                bucket_name,
                file
                )
            print('After uploading')
            return {'detail' : f'file with path {files} successfully uploaded'}
        except Exception as e:
            print(str(e))

        #####download files######

        client.download_file(bucket_name,'logo1.jpg',os.path.join('./FilesDownload', 'test.jpg'))


list = client.list_objects(Bucket = bucket_name )['Contents']
            for key in list:
                client.download_file(bucket_name,key['Key'], os.path.join('./FilesDownload', key['Key']))

            return {'detail' : f'file with path {files} successfully uploaded'}
        except Exception as e:
            print(str(e))

    
'''
