from fastapi import APIRouter, Depends, status, HTTPException
from datetime import datetime, timedelta
from jose import jwt, JWTError
from passlib.context import CryptContext
from fastapi.security import OAuth2PasswordBearer
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
import models
from config.db import  fetch_user, user_collection
import os
from dotenv import load_dotenv, find_dotenv


router = APIRouter(
    tags=['login'],
    prefix="/login"
)

load_dotenv(find_dotenv())

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

SECRET_KEY = os.getenv('SECRET_KEY')
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 20
oauth2_scheme = OAuth2PasswordBearer(tokenUrl='login')


async def generate_token(data : dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp" : expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm = ALGORITHM)
    return encoded_jwt

@router.post('/')
async def login(request: OAuth2PasswordRequestForm = Depends() ):
    user = await user_collection.find_one({'name' : request.username})
    print(user['name'])

    if not user:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail="Username not found .")
    
    if not pwd_context.verify(request.password, user['password']):
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail="Password Incorrect .")

    access_token = await generate_token(
        data = {"sub" : user['name'], "email" : user['email']}
    )
    return {"access_token" : access_token, "token_type" : "bearer"}

async def get_current_user(token : str = Depends(oauth2_scheme)):
    credential_exception = HTTPException(
        status_code = status.HTTP_401_UNAUTHORIZED,
        detail = "Invalid Auth Credentials",
        headers = {'WWW-Authenticate' : "Bearer"}
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username : str = payload.get("sub")
        print('----',username)
        if username is None:
            raise credential_exception
        token_data = models.user.TokenData(username = username)
        user = await user_collection.find_one({'name' : token_data.username})

        return user
    except JWTError:
        raise credential_exception