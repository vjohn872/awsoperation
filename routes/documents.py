from fastapi import APIRouter,  HTTPException, status, Depends
from fastapi.encoders import jsonable_encoder
from passlib.context import CryptContext
from models.user import Document, Document_Details, ShowDocument_Details
from config.db1 import create_document_detail,fetch_all_document_detail,fetch_document_byid, fetch_document_bycompanyid,fetch_document_byparamsid, fetch_document_bysiteid
from .login import get_current_user
from typing import List


documents = APIRouter(
    prefix = '/platform/v2/manualDocument',
    tags = ["Document_Details"]
)



@documents.post('/create')
async def add_document_details(document : Document_Details):
    paths = []
    for file in document.files:
        path = 's3://verma.s/' + file
        print(path)
        paths.append(path)
    print(paths)
    document.files = paths
    document = jsonable_encoder(document)
    new_document = await create_document_detail(document)
    return {'data' : new_document}

@documents.get('/')
async def find_all_documentss():
    documents = await fetch_all_document_detail()
    if not documents:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail= "Empty collection - no document found")
    return documents

@documents.get('/{id}')
async def find_documents_Byid(id):
    document = await fetch_document_byid(id)
    if not document:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail= "Empty collection - no document found")
    return document

@documents.get('/getbycompanyid/{companyid}')
async def find_documents_bycompanyid(companyid : str):
    print("+++++")
    document = await fetch_document_bycompanyid(companyid)
    if not document:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail= "Empty collection - no document found")
    return document

@documents.get('/getbysiteid/{siteid}')
async def find_documents_bysiteid(siteid : str):
    print("+++++")
    document = await fetch_document_bysiteid(siteid)
    if not document:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail= "Empty collection - no document found")
    return document



@documents.get('/{company_id}/getbymultipleparams/')
async def find_documents_byparamsid(company_id, siteid : str, title: str):
    print("+++++")
    document = await fetch_document_byparamsid(siteid,title,company_id)
    if not document:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail= "Empty collection - no document found")
    return document

