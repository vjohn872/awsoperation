
from fastapi import APIRouter,  HTTPException, status, Depends
from fastapi.encoders import jsonable_encoder
from passlib.context import CryptContext
from models.user import User, ShowUser, UpdateUser
from config.db import create_user, fetch_all_users, updated_user, fetch_user, delete_user
from .login import get_current_user
from models import user


user = APIRouter(
    prefix = '/user',
    tags = ["Users"]
)


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")



@user.get('/')
async def find_all_users(current_user : User = Depends(get_current_user)):
    print(current_user['name'])
    print("==",current_user.get('password'))
    print("=++=",current_user.get('email'))
    users = await fetch_all_users()
    if not users:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail= "Empty collection - no user found")
    return users


@user.get('/{id}', response_model= ShowUser)
async def get_user(id):
    user = await fetch_user(id)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail= f"User with id - {id}, Not found")
    return user
'''
@user.get('/{id}')
async def find_one_user(id):
    return serializeDict(conn.local.user.find_one({"_id":ObjectId(id)}))
'''


@user.post('/create')
async def add_user(user : User):
    hashedpass = pwd_context.hash(user.password)
    user.password = hashedpass
    user = jsonable_encoder(user)
    new_user = await create_user(user)
    return {'data' : new_user}


@user.put('/{id}')
async def update(id : str, request : UpdateUser):
    req = {k: v for k, v in request.dict().items() if v is not None}
    update_user = await updated_user(id, req)
    if not update_user:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail = "Update could not be completed")
    return f"Record with id - {id}, Updated successfully"

@user.delete('/{id}')
async def drop(id):
    deleted_user = await delete_user(id)
    if deleted_user:
        return {'detail' : f'user with id {id} successfully deleted'}
    raise HTTPException(status.HTTP_404_NOT_FOUND, detail= "user not found in the db")

"""

####################UPLOADING FILES TO AWS S3 BUCKET################################

@user.post('/upload-files', status_code=201)
async def file_upload(files : List[UploadFile] = File(...)):
    client = boto3.client(
        's3',
        aws_access_key_id = access_key,
        aws_secret_access_key = secret_access_key      
    )
    #######upload files#######

    #files = os.path.join(os.getcwd(),'Media')
    print(files)
    for file in files:

        fil_loc = os.path.join(os.getcwd(),'Media/') + file.filename
        with open(fil_loc, 'wb') as file1:
            shutil.copyfileobj(file.file, file1)
    
        try:
            print('before uploading')
            client.upload_file(
                   fil_loc,
                   bucket_name,
                   file.filename
                   )
            
            print('After uploading')

            list = client.list_objects(Bucket = bucket_name )['Contents']
            for key in list:
                client.download_file(bucket_name,key['Key'], os.path.join('./FilesDownload', key['Key']))

            return {'detail' : f'file with path {files} successfully uploaded'}
        except Exception as e:
            print(str(e))

        #####download files######
        
        #client.download_file(bucket_name,'logo1.jpg',os.path.join('./FilesDownload', 'test.jpg'))
        try:
            list = client.list_objects(Bucket = bucket_name )['Contents']
            for key in list:
                client.download_file(bucket_name,key['Key'], os.path.join('./FilesDownload', key['Key']))
        except Exception as e:
            print(str(e))

        


'''
@user.post('/upload-files')
async def file_upload():
    client = boto3.client(
        's3',
        aws_access_key_id = access_key,
        aws_secret_access_key = secret_access_key      
    )
    #######upload files#######

    files = os.path.join(os.getcwd(),'Media')
    print(files)
    for file in os.listdir(files):
        print(type(file))
        try:
            print('before uploading')
            client.upload_file(
                os.path.join(files,file),
                bucket_name,
                file
                )
            print('After uploading')
            return {'detail' : f'file with path {files} successfully uploaded'}
        except Exception as e:
            print(str(e))

        #####download files######

        client.download_file(bucket_name,'logo1.jpg',os.path.join('./FilesDownload', 'test.jpg'))

    
'''

        


'''
@user.put('/{id}')
async def update_user(id, user : User):
    conn.local.user.find_one_and_update({"_id":ObjectId(id)}, {
        "$set" : dict(user)
    })
    return serializeDict(conn.local.user.find_one({"_id":ObjectId(id)}))

@user.delete('/{id}')
async def delete_user(id):
    return serializeDict(conn.local.user.find_one_and_delete({"_id":ObjectId(id)}))

'''

"""