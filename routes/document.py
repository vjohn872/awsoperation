from fastapi import APIRouter,  HTTPException, status, Depends
from fastapi.encoders import jsonable_encoder
from passlib.context import CryptContext
from models.user import Document, Document_Details, ShowDocument_Details
from config.db1 import create_document_detail,fetch_all_document_detail,fetch_document_byid, fetch_document_bycompanyid,fetch_document_byparamsid, fetch_document_bysiteid
from .login import get_current_user
from typing import List


documents = APIRouter(
    prefix = '/platform/v2/manualDocument',
    tags = ["Document_Details"]
)